package com.company.service;

import com.company.dao.TransactionDaoImpl;
import com.company.dao.TransactionDao;
import com.company.model.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 27.11.2016.
 */
public class TransactionService {

    TransactionDao transactionDao = new TransactionDaoImpl();

    public Transaction get(Long id) throws Exception {

        Transaction transaction = transactionDao.get(id);

        return transaction;
    }

    Long save(Transaction transaction) {

        Long result = transactionDao.save(transaction);

        return result;
    }

    void update(Transaction transaction) {
        transactionDao.update(transaction);
    }

    public void delete(Long id) throws Exception {
        transactionDao.delete(id);
    }

    public List<Transaction> list(Long id) {

        List<Transaction> list = new ArrayList<>();
        list = transactionDao.list(id);

        return list;
    }

    void deleteByAccount(Long accountId) {
        transactionDao.deleteByAccount(accountId);
    }
}
