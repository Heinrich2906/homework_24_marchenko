package com.company.service;

import com.company.dao.AccountDao;
import com.company.dao.AccountDaoImpl;
import com.company.model.Account;
import com.company.model.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 27.11.2016.
 */
public class AccountsService {

    AccountDao accountDao = new AccountDaoImpl();

    Account get(Long accountNumber) throws Exception {

        Account account = accountDao.get(accountNumber);

        return account;
    }

    Long save(Account account) {

        boolean flag = false;
        if (account.getAccountNumber() == null) flag = true;

        AccountDao accountDao = new AccountDaoImpl();
        Long result = accountDao.save(account);

        if (flag && (account.getBalance().compareTo(new BigDecimal("0")) == 1)) {

            Transaction transaction = new Transaction(null,
                    account.getBalance(),
                    Timestamp.valueOf(LocalDateTime.now()),
                    "PUT",
                    account.getAccountNumber());

            TransactionService transactionService = new TransactionService();
            transactionService.save(transaction);
        }

        return result;
    }

    void update(Account account) {
        accountDao.update(account);
    }

    public void delete(Long accountNumber) throws Exception {
        accountDao.delete(accountNumber);
    }

    public List<Account> list(Long id) {

        List<Account> list = new ArrayList<>();
        list = accountDao.list(id);

        return list;
    }

    void deleteByCustomer(Long customerId) throws Exception {
        accountDao.deleteByCustomer(customerId);
    }

    //Вариант 2, день рождения - TimeStamp
    public void create(BigDecimal balance,
                       Timestamp creationDate,
                       String currency,
                       boolean blocked,
                       Long customerId) {

        Account account = new Account(0L, balance, creationDate, currency, blocked, customerId);
        Long accountNumber = this.save(account);
    }
}
