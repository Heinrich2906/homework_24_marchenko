package com.company.dao;

import com.company.model.Transaction;
import java.util.List;

/**
 * Created by heinr on 30.11.2016.
 */
public interface TransactionDao {

    Transaction get(Long id) throws Exception;
    Long save(Transaction transaction);
    void update(Transaction transaction);
    void delete(Long id);
    boolean deleteByAccount(Long accountId);
    List<Transaction> list(Long id);
}
