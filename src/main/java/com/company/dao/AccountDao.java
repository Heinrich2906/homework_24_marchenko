package com.company.dao;

import com.company.model.Account;
import java.util.List;

/**
 * Created by heinr on 27.11.2016.
 */
public interface AccountDao{

    Account get(Long accountNumber) throws Exception;
    Long save(Account account);
    void update(Account account);
    boolean delete(Long accountNumber) throws Exception;
    boolean deleteByCustomer(Long customerId) throws Exception;
    List<Account> list(Long id);

}