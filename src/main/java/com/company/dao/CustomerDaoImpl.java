package com.company.dao;

import com.company.util.GetConnection;
import com.company.model.Customer;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

/**
 * Created by heinr on 30.11.2016.
 */
public class CustomerDaoImpl implements CustomerDao {

    public Customer get(Long id) {

        Connection connection = null;
        Customer customer = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE id = ?");

            //2.1. Передача параметра
            statement.setLong(1, id);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                customer = new Customer(resultSet.getLong("id"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getTimestamp("birthDay"),
                        resultSet.getString("address"),
                        resultSet.getString("city"),
                        resultSet.getString("passport"),
                        resultSet.getString("phone"));
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customer;
    }

    public Long save(Customer customer) {

        Connection connection = null;
        boolean flag = false;
        long result = 0L;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            Long customerId = customer.getId();
            PreparedStatement statement = null;
            ResultSet resultSet = null;

            if (customerId != null) {

                //2. Создание запроса
                statement = connection.prepareStatement("SELECT FIRST 1 id FROM customers WHERE id = ?");

                //2.1. Передача параметра
                statement.setLong(1, customerId);

                //3. Выполнение запроса
                resultSet = statement.executeQuery();

                //4. Обработка результата запроса
                while (!resultSet.isLast() && resultSet.next()) {

                    flag = true;

                    // Шаг 5.1. Закрыть ResultSet
                    resultSet.close();

                    this.update(customer);

                    result = customer.getId();
                }
            }

            if (!flag) {

                statement = connection.prepareStatement("INSERT INTO customers (firstname, lastname, birthday, address, city, passport, phone) VALUES(?,?,?,?,?,?,?)");

                statement.setString(1, customer.getFirstName());
                statement.setString(2, customer.getLastName());
                statement.setTimestamp(3, customer.getBirthDay());
                statement.setString(4, customer.getAddress());
                statement.setString(5, customer.getCity());
                statement.setString(6, customer.getPassport());
                statement.setString(7, customer.getPhone());

                int blank = 0;
                blank = statement.executeUpdate();

                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    // Получаем поле id
                    result = resultSet.getLong("id");
                }

            }

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void update(Customer customer) {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("UPDATE customers SET firstName = ?,lastName = ?, birthDay = ?, address = ?, city = ?, passport = ?, phone = ? where id = ?");

            //2.1. Передача параметра
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setTimestamp(3, customer.getBirthDay());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getPassport());
            statement.setString(7, customer.getPhone());
            statement.setLong(7, customer.getId());

            //3. Выполнение запроса
            int blank = statement.executeUpdate();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Customer> list() {

        Connection connection = null;
        List<Customer> accountList = new ArrayList<>();

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery("SELECT * FROM customers");

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                Customer customer = new Customer(resultSet.getLong("id"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getTimestamp("birthDay"),
                        resultSet.getString("address"),
                        resultSet.getString("city"),
                        resultSet.getString("passport"),
                        resultSet.getString("phone"));

                accountList.add(customer);

            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return accountList;
    }

    public void delete(Long customerId) throws Exception {

        boolean result = false;

        AccountDao accountDao = new AccountDaoImpl();
        result = accountDao.deleteByCustomer(customerId);

        if (result) {

            Connection connection = null;

            try {
                //1. Зарегистрировать драйвер БД в менеджере
                connection = GetConnection.getConnection();

                //2. Создание запроса
                PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?");

                //2.1. Передача параметра
                statement.setLong(1, customerId);

                //3. Выполнение запроса
                int blank = statement.executeUpdate();

                // Шаг 5.2. Закрыть Statement
                statement.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // Шаг 5.3. Закрыть соединение
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

