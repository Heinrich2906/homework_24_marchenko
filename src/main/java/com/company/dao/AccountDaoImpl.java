package com.company.dao;

import com.company.util.GetConnection;
import com.company.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

/**
 * Created by heinr on 30.11.2016.
 */
public class AccountDaoImpl implements AccountDao {

    public Account get(Long accountNumber) {

        Connection connection = null;
        Account account = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM accounts WHERE accountNumber = ?");

            //2.1. Передача параметра
            statement.setLong(1, accountNumber);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                account = new Account(resultSet.getLong("accountNumber"),
                        resultSet.getBigDecimal("balance"),
                        resultSet.getTimestamp("creationDate"),
                        resultSet.getString("currency"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getLong("customerId"));

            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return account;
    }

    public Long save(Account account) {

        Connection connection = null;
        boolean flag = false;
        Long result = 0L;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            Long accountNumber = account.getAccountNumber();
            PreparedStatement statement = null;
            ResultSet resultSet = null;

            if (accountNumber != 0L) {

                //2. Создание запроса
                statement = connection.prepareStatement("SELECT FIRST 1 accountNumber FROM accounts WHERE accountNumber = ?");

                //2.1. Передача параметра
                statement.setLong(1, account.getAccountNumber());

                //3. Выполнение запроса
                resultSet = statement.executeQuery();

                //4. Обработка результата запроса
                while (!resultSet.isLast()) {

                    resultSet.next();
                    flag = true;

                    // Шаг 5.1. Закрыть ResultSet
                    resultSet.close();

                    this.update(account);

                    result = account.getAccountNumber();
                }
            }

            if (!flag) {

                statement = connection.prepareStatement("INSERT INTO accounts (balance, creationDate, currency, blocked, customerId) VALUES(?,?,?,?,?)");

                statement.setBigDecimal(1, account.getBalance());
                statement.setTimestamp(2, account.getCreationDate());
                statement.setString(3, account.getCurrency());
                statement.setBoolean(4, account.isBlocked());
                statement.setLong(5, account.getCustomerId());

                int blank = statement.executeUpdate();

                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    // Получаем поле id
                    result = resultSet.getLong("accountNumber");
                }

            }

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public void update(Account account) {

        Connection connection = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("UPDATE accounts SET balance = ?,creationDate = ?, currency = ?, blocked = ?, customerId= ? where accountNumber = ?");

            //2.1. Передача параметра
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.isBlocked());
            statement.setLong(5, account.getCustomerId());
            statement.setLong(6, account.getAccountNumber());

            //3. Выполнение запроса
            int blank = statement.executeUpdate();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Account> list(Long id) {

        Connection connection = null;
        List<Account> accountList = new ArrayList<>();

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            Statement statement = connection.createStatement();

            ResultSet resultSet = null;

            //3. Выполнение запроса
            if (id == -1L) {
                resultSet = statement.executeQuery("SELECT * FROM accounts");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM accounts WHERE customerId=" + id.toString());
            }

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {

                resultSet.next();

                Account account = new Account(resultSet.getLong("accountNumber"),
                        resultSet.getBigDecimal("balance"),
                        resultSet.getTimestamp("creationDate"),
                        resultSet.getString("currency"),
                        resultSet.getBoolean("blocked"),
                        resultSet.getLong("customerId"));

                accountList.add(account);

            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return accountList;
    }

    public boolean delete(Long accountNumber) throws Exception {

        Connection connection = GetConnection.getConnection();
        boolean result = false;

        if (connection != null) {
            // Если соединение пришло в метод извне, а не было создано локально,
            // нужно запомнить значение автокоммита и восстановить его в конце
            boolean oldAutocommit = connection.getAutoCommit();

            // Выключаем автокоммит запросов.
            // Результаты запросов не будут записаны в базу, пока не выполнится ручной коммит
            // Также коммит произойдёт, если внутри транзакции будет вызван метод setAutoCommit с любым аргументом.
            connection.setAutoCommit(false);

            PreparedStatement statement = null;

            try {
                //1.Сначала удаляю все транзакции по счёту.
                //2. Удаляю сам счёт.

                // Выполняем запросы. При autoCommit = false они будут сгруппированы в одну транзакцию.
                TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();

                result = transactionDaoImpl.deleteByAccountCommon(accountNumber, connection);

                if (result) {
                    statement = connection.prepareStatement("DELETE FROM accounts WHERE accountNumber = ?");

                    //2.1. Передача параметра
                    statement.setLong(1, accountNumber);

                    //3. Выполнение запроса
                    int blank = statement.executeUpdate();
                }
                // Пытаемся закоммитить транзакцию. Если все запросы и вызов commit() прошли успешно,
                // результаты будут записаны в базу
                connection.commit();

            } catch (SQLException e) {
                e.printStackTrace();
                // Если один из запросов или вызов commit() выбросит исключение,
                // откатываем транзакцию. Все запросы, которые были выполнены внутри транзакции будут отменены.
                connection.rollback();
            } finally {
                if (statement != null) {
                    statement.close();
                }

                // Восстанавливаем значение автокоммита, если соединение пришло извне
                connection.setAutoCommit(oldAutocommit);

                // Если соединение создано локально, закрываем его
                connection.close();
            }

        }
        return result;
    }

    public boolean deleteByCustomer(Long customerId) throws Exception {

        Connection connection = null;
        boolean result = true;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT accountNumber FROM accounts WHERE customerId = ?");

            //2.1. Передача параметра
            statement.setLong(1, customerId);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast() && resultSet.next()) {

                boolean tempResult = false;

                Account account = this.get(resultSet.getLong("accountNumber"));
                tempResult = this.delete(account.getAccountNumber());

                if (!result) {
                    result = tempResult;
                }
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            result = false;
            e.printStackTrace();
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
