package com.company.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by heinr on 27.11.2016.
 */
public class Account {

    private Long accountNumber;
    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private boolean blocked;
    private Long customerId;

    public Account(Long accountNumber, BigDecimal balance, Timestamp creationDate, String currency, boolean blocked, Long customerId) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.creationDate = creationDate;
        this.currency = currency;
        this.blocked = blocked;
        this.customerId = customerId;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Account number " + this.accountNumber + "(creation date " + this.creationDate + ") ID customer " + this.customerId + " is blocked " + this.blocked + " currency " + this.currency + " balance " + this.balance;
    }

}
