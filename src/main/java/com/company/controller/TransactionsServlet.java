package com.company.controller;

import com.company.model.Transaction;
import com.company.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by heinr on 18.12.2016.
 */
public class TransactionsServlet extends HttpServlet {

    private TransactionService transactionService = new TransactionService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/transactions")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        resp.setCharacterEncoding("utf-8");

        Long accountId = Long.parseLong(req.getParameter("id"));
        Boolean answerNo = new Boolean(false);
        Boolean answerYes = new Boolean(true);

        req.setAttribute("transactions", transactionService.list(accountId));

        req.setAttribute("isTransactionActive", answerYes);
        req.setAttribute("isCustomersActive", answerNo);
        req.setAttribute("isAccountActive", answerNo);
        req.setAttribute("showHouse", answerYes);

        req.getRequestDispatcher("/WEB-INF/jsp/transactions.jsp").forward(req, resp);
    }

    // Обработчик POST запросов на
    // http://localhost:8080/transactions/create
    // и
    // http://localhost:8080/transactions/delete
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "delete":
                // Достаем поле id из формы удаления кастомера и удаляем соответствующую запись через сервис
                try {
                    Long id = Long.parseLong(params.get("id")[0]);
                    transactionService.delete(id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

        resp.sendRedirect("/transactions?id=-1");
    }
}
