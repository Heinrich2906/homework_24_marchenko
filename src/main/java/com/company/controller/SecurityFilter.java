package com.company.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by heinr on 25.12.2016.
 */
public class SecurityFilter implements Filter {

    // То же, что init у сервлета. Инициализация фильтра.
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    // То же, что destroy у сервлета.
    @Override
    public void destroy() {
    }

    // Основной метод фильтра.
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // Получаем сессию
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpSession session = httpReq.getSession();

        // Если в сессии есть параметр username, то пропускаем запрос дальше
        if (session.getAttribute("username") != null || ((HttpServletRequest) request).getRequestURI().equals("/login")) {
            // Обязательно вызываем chain.doFilter, чтобы пропустить запрос дальше
            chain.doFilter(request, response);
        } else {
            // Иначе перенаправляем пользователя на сраницу логина
            HttpServletResponse httpResp = (HttpServletResponse) response;
            httpResp.sendRedirect("/login");
        }
    }
}
