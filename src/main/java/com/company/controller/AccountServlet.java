package com.company.controller;

import com.company.service.AccountsService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.util.Map;

/**
 * Created by heinr on 17.12.2016.
 */
public class AccountServlet extends HttpServlet {

    private AccountsService accountsService = new AccountsService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/accounts")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        resp.setCharacterEncoding("utf-8");

        Long customerId = Long.parseLong(req.getParameter("id"));
        Boolean answerNo = new Boolean(false);
        Boolean answerYes = new Boolean(true);

        req.setAttribute("accounts", accountsService.list(customerId));
        req.setAttribute("customerId", customerId);

        req.setAttribute("isTransactionActive", answerNo);
        req.setAttribute("isCustomersActive", answerNo);
        req.setAttribute("isAccountActive", answerYes);
        req.setAttribute("showHouse", answerYes);

        req.getRequestDispatcher("/WEB-INF/jsp/accounts.jsp").forward(req, resp);
    }

    // Обработчик POST запросов на
    // http://localhost:8080/customers/create
    // и
    // http://localhost:8080/customers/delete
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "delete":
                // Достаём поле id из формы удаления кастомера и удаляем соответствующую запись через сервис
                try {
                    Long accountNumber = Long.parseLong(params.get("id")[0]);
                    accountsService.delete(accountNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

        resp.sendRedirect("/customers");
    }
}
