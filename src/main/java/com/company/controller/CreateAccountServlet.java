package com.company.controller;

import com.company.service.AccountsService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

/**
 * Created by heinr on 07.01.2017.
 */
public class CreateAccountServlet extends HttpServlet {

    private AccountsService accountsService = new AccountsService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/createAccount")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        // Генерируем ответ и записываем его в response
        // Подробнее см. IndexServlet
        resp.setCharacterEncoding("utf-8");

        //req.setAttribute("account", accountsService.list(-1L));

        String customerId = req.getParameter("id");
        Boolean answerNo = new Boolean(false);
        Boolean answerYes = new Boolean(true);

        req.setAttribute("customerId", customerId);
        req.setAttribute("isTransactionActive", answerNo);
        req.setAttribute("isCustomersActive", answerYes);
        req.setAttribute("isAccountActive", answerNo);
        req.setAttribute("showHouse", answerYes);

        req.getRequestDispatcher("/WEB-INF/jsp/createNewAccount2.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        String stringCustomerId = req.getParameter("customerId");

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "create":

                Date date = new Date();
                Timestamp timeStampDate = new Timestamp(date.getTime());

                Long customerId = Long.parseLong(stringCustomerId);
                BigDecimal bigDecimal = new BigDecimal(0);

                accountsService.create(
                        // Достаём значения полей формы из параметров запроса
                        bigDecimal,
                        timeStampDate,
                        params.get("currency")[0],
                        false,
                        customerId);

                break;
        }

        resp.sendRedirect("/accounts?id=" + stringCustomerId);
    }
}
