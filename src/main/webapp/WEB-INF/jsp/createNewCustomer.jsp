<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.company.model.Customer" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <%@ include file="footer.jsp" %>

        <div class="header">
            Neukunde
        </div>

        <div id="form-container">
            <form action="createCustomer/create" method="post" id="customer-form">

            <label for="fname">Vorname</label>
            <input class="input-box" type="text" id="fname" name="firstname" placeholder="First Name">

            <label for="lname">Nachname</label>
            <input class="input-box" type="text" id="lname" name="lastname" placeholder="Last Name">

            <label for="birthdate">Geburtstag</label>
            <input class="input-box" type="date" id="birthdate" name="birthdate" placeholder="Birthdate">

            <label for="address">Adresse</label>
            <input class="input-box" type="text" id="address" name="address" placeholder="Address">

            <label for="city">Stadt</label>
            <input class="input-box" type="text" id="city" name="city" placeholder="City">

            <label for="passport">Ausweis</label>
            <input class="input-box" type="text" id="passport" name="passport" placeholder="Passport">

            <label for="phone">Phone</label>
            <input class="input-box" type="text" id="phone" name="phone" placeholder="Phone">

            <button type='submit' class="btn">Erstellen</a>

        </div>

        <script src="/javascript/script.js"></script>

    </body>

</html>

