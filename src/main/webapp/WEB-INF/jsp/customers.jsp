<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.company.model.Customer" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <%@ include file="footer.jsp" %>

        <div class="header">
            Kunden
        </div>

        <div>
            <a href="/createCustomer" class="btn">Erstellen neuer Kunden</a>
        </div>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Geburtstag</th>
                        <th>Adresse</th>
                        <th>Stadt</th>
                        <th>Ausweis</th>
                        <th>Phone</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                <% List<Customer> customers = (List<Customer>)request.getAttribute("customers"); %>
                <%for (int i = 0; i < customers.size(); i++){ %>
                    <% Customer customer = customers.get(i); %>
                    <% String customerId = customer.getId().toString(); %>
                    <tr>
                        <td><a href="/accounts?id=<%= customerId %>"><%= customer.getFirstName()%></a></td>
                        <td><%= customer.getLastName()%></td>
                        <td><%= customer.getBirthDay()%></td>
                        <td><%= customer.getAddress()%></td>
                        <td><%= customer.getCity()%></td>
                        <td><%= customer.getPassport()%></td>
                        <td><%= customer.getPhone()%></td>
                        <td>
                            <form action="customers/delete" method="post">
                                <input type="hidden" name="id" value=<%= customerId %> />
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                    <%}%>
                </tbody>

            </table>

        </div>

    </body>

</html>
