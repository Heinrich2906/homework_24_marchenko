<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.company.model.Transaction" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <%@ include file="footer.jsp" %>

        <div class="header">
            Transakionen
        </div>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Accountnumber</th>
                        <th>Vorgangs-ID</th>
                        <th>Buchungstransaktionen</th>
                        <th>Datum</th>
                        <th>Balance</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                <% List<Transaction> transactions = (List<Transaction>)request.getAttribute("transactions");%>
                <%for (int i = 0; i < transactions.size(); i++){ %>
                 <% Transaction transaction = transactions.get(i); %>
                 <% Long transactionId = transaction.getAccountNumber(); %>
                    <tr>
                        <td><%= transactionId %></td>
                        <td><%= transaction.getId() %></td>
                        <td><%= transaction.getOperationType() %></td>
                        <td><%= transaction.getDate() %></td>
                        <td><%= transaction.getAmount() %></td>
                        <td>
                            <form action="transactions/delete" method="post">
                                <input type="hidden" name="id" value=<%= transactionId.toString() %>/>
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                    <%}%>
                </tbody>

            </table>

        </div>

    </body>

</html>
