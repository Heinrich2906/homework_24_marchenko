<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <%@ include file="footer.jsp"%>

        <div class="container">
            <div class="header">
                Willkommen bei der Bank!
            </div>
        </div>

    </body>

</html>
