<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.company.model.Account" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <%@ include file="footer.jsp"%>

        <div class="header">
            Konten
        </div>

        <% Long customerId = (Long) request.getAttribute("customerId");%>
        <% if(customerId != -1L) { %>
            <div>
                <a href="/createAccount?id=<%=customerId.toString() %>" class="btn">Erstellen ein neues Konto</a>
            </div>
        <% } %>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Accountnumber</th>
                        <th>Balance</th>
                        <th>Erstellungsdatum</th>
                        <th>Währung</th>
                        <th>Ist blockiert</th>
                        <th>Kundennummer</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                <% List<Account> accounts = (List<Account>)request.getAttribute("accounts");%>
                <%for (int i = 0; i < accounts.size(); i++){ %>
                    <% Account account = accounts.get(i); %>
                    <% String accountId = account.getAccountNumber().toString(); %>
                    <tr>
                        <td><a href="/transactions?id=<%= accountId %>"><%= accountId %></a></td>
                        <td><%= account.getBalance()%></td>
                        <td><%= account.getCreationDate()%></td>
                        <td><%= account.getCurrency()%></td>
                        <td><%= account.isBlocked()%></td>
                        <td><%= account.getCustomerId()%></td>
                        <td>
                            <form action="accounts/delete" method="post">
                                <input type="hidden" name="id" value=<%= accountId %>/>
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                    <%}%>
                </tbody>

            </table>

        </div>

    </body>

</html>
