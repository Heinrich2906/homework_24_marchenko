<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <body class="container">
            <form action='/login' method='post'>
            <input type='text' name='login' placeholder='Login'/>
            <input type='password' name='password' placeholder='Password'/>
            <button type='submit'>Login</button>
            </form>
        </body>

    </body>

</html>
