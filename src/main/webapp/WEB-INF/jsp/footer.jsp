<%@ page import="java.time.LocalDateTime" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul>
    <% Boolean isCustomersActive = (Boolean) request.getAttribute("isCustomersActive");%>
    <% if(isCustomersActive.booleanValue()) { %>
        <li><a class="active" href="/customers">Customers</a></li>
    <% } %>
    <% if(!isCustomersActive.booleanValue()) { %>
        <li><a href="/customers">Customers</a></li>
    <% } %>

    <% Boolean isAccountActive = (Boolean) request.getAttribute("isAccountActive");%>
    <% if(isAccountActive.booleanValue()) { %>
        <li><a class="active" href="/accounts?id=-1">Accounts</a></li>
    <% } %>
    <% if(!isAccountActive.booleanValue()) { %>
        <li><a href="/accounts?id=-1">Accounts</a></li>
    <% } %>

    <% Boolean isTransactionActive = (Boolean) request.getAttribute("isTransactionActive");%>
    <% if(isTransactionActive.booleanValue()) { %>
        <li><a class="active" href="/transactions?id=-1">Transactions</a></li>
    <% } %>
    <% if(!isTransactionActive.booleanValue()) { %>
        <li><a href="/transactions?id=-1">Transactions</a></li>
    <% } %>

    <% Boolean showHouse = (Boolean) request.getAttribute("showHouse");%>
    <% if(showHouse.booleanValue()) { %>
        <ul  class="home">
            <li><a href="/index"><img src="../img/home.png"></a></li>
        </ul>
    <% } %>
</ul>
